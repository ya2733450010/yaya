
let userPrimaryKey = localStorage.getItem('primaryKey')
let maxImg = document.getElementsByClassName('swper')[0]
let {oneId,twoId,threeId} = JSON.parse(localStorage.getItem('paramsId'))
let dataList = []
let mySwiper = ''
let zoom = { zoom: false }
function getImgs(params) {
  let imgs = [
    './imgs/u=1089874897,1268118658&fm=26&gp=0.jpg',
    './imgs/u=1830914723,3154965800&fm=26&gp=0.jpg',
    './imgs/u=1906469856,4113625838&fm=26&gp=0.jpg',
    './imgs/u=2534506313,1688529724&fm=26&gp=0.jpg',
    './imgs/u=3238317745,514710292&fm=26&gp=0.jpg',
    './imgs/u=3363295869,2467511306&fm=26&gp=0.jpg',
    './imgs/u=3892521478,1695688217&fm=26&gp=0.jpg'
  ]
        initEle(imgs)
        dataList = imgs
}
getImgs()

function initEle(data) {
  let remark = ''
  let scrollBox = document.getElementsByClassName('scroll')[0]
  for (let i = 0; i < data.length; i++) {
    const element = data[i];
      maxImg.innerHTML =  maxImg.innerHTML +
        '<div class="swiper-slide max-img" onclick="preview()">'+
          '<img src="'+
          element +
          '" alt="">'
        '</div>'

      scrollBox.innerHTML = scrollBox.innerHTML +
      '<div class="img-item '+ (i == 0? 'active' : '') +'">'+
        '<img src="'+ element +'" alt="" onclick="clickItemImg(this,'+ i +')">'+
      '</div>'
  }
  
  // 轮播

  mySwiper = new Swiper ('.swiper-container', {
    zoom: zoom.zoom,
    on:{
      slideChange(){
        clickItemImg('',this.activeIndex)
      }
    }
  }) 
}
let maxImgChildrenImg = document.getElementsByClassName('swper')[0].children[0]
let maxImgChildrenText = document.getElementsByClassName('swper')[0].children[1]
let scrollBox = document.getElementsByClassName('scroll')[0]
let imgTabs = document.getElementsByClassName('img-tabs')[0]
function clickItemImg(e, index ) {
  mySwiper.slideTo(index, 100, false);    
  for (let i = 0; i < scrollBox.children.length; i++) {
    const element = scrollBox.children[i];
    if(i == index){
      element.classList.add('active')
    }else{
      element.classList.remove('active')
    }
  }
  // 位移
  if(index > 1){
    scrollBox.scrollLeft = '200' * (index-2)
  }else if(index == 2){
    scrollBox.scrollLeft = 0
  }
}
// 点击预览
function preview(params) {
  if(imgTabs.style.display == 'none'){
    imgTabs.style.display = 'block'
    zoom.zoom = false
  }else{
    imgTabs.style.display = 'none'
    zoom.zoom = true
  }
}